FROM python:3.9
WORKDIR /fly
COPY requirements.txt /fly/
RUN pip install -r requirements.txt
COPY . /fly
CMD python blazebot
