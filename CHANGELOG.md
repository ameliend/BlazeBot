## [2.3.1](https://gitlab.com/ameliend/BlazeBot/compare/v2.3.0...v2.3.1) (2023-06-21)


### :scissors: Refactor

* removed some custom reactions ([290f6da](https://gitlab.com/ameliend/BlazeBot/commit/290f6da0a269808cb386aa255f7e733ea916eecf))


### 📔 Docs

* update README ([b2c46c1](https://gitlab.com/ameliend/BlazeBot/commit/b2c46c1925cf42afe6ea35ea276a0fd6bc501ace))
* update README ([125d638](https://gitlab.com/ameliend/BlazeBot/commit/125d63818ad11e845d08849e3702b40af9653885))


### 🦊 CI/CD

* update classifiers ([50a2d54](https://gitlab.com/ameliend/BlazeBot/commit/50a2d546717fe5ec708658b59cc28959c6e80d96))
* update requirements versions ([d9f93b9](https://gitlab.com/ameliend/BlazeBot/commit/d9f93b9916f434dbf7fed832c3fc0bbe25967a64))
* added gitlab templates ([d8aad6b](https://gitlab.com/ameliend/BlazeBot/commit/d8aad6b32583e5a212077ce095d7fdbcad7fd755))


### 🛠 Fixes

* add cleverbot ignore message ([a7400f3](https://gitlab.com/ameliend/BlazeBot/commit/a7400f3203ccb8a3a6e5aaf2dda857f6fce9d968))
* wrong arguments ([6e79a3f](https://gitlab.com/ameliend/BlazeBot/commit/6e79a3fbb3e70a0f0097d2b0ffd7cea951e94cb6))


### Other

* removed python2 coding arguments ([fbc2de1](https://gitlab.com/ameliend/BlazeBot/commit/fbc2de1557ce52f6926555a45913dc572350e56e))
* update pylint config ([16eeaec](https://gitlab.com/ameliend/BlazeBot/commit/16eeaeccd623486101cbf438491ac0567cde3478))

## [2.3.0](https://gitlab.com/ameliend/BlazeBot/compare/v2.2.2...v2.3.0) (2023-03-31)


### 🚀 Features

* add more custom reactions ([97a59d6](https://gitlab.com/ameliend/BlazeBot/commit/97a59d60f2d417b04c9d7b8f5b9099d9156d6b89))

## [2.2.2](https://gitlab.com/ameliend/BlazeBot/compare/v2.2.1...v2.2.2) (2023-03-31)


### 🛠 Fixes

* fixing some custom triggers ([74d9d74](https://gitlab.com/ameliend/BlazeBot/commit/74d9d74beeadfb5b0ad9138ee5eb986947d129ec))

## [2.2.1](https://gitlab.com/ameliend/BlazeBot/compare/v2.2.0...v2.2.1) (2023-03-31)


### 🛠 Fixes

* spelling mistakes ([f194385](https://gitlab.com/ameliend/BlazeBot/commit/f1943857695196a6f39963f101d8ca5c462d4f7a))

## [2.2.0](https://gitlab.com/ameliend/BlazeBot/compare/v2.1.1...v2.2.0) (2023-03-31)


### 🦊 CI/CD

* removed fly.io pipeline ([3d9566f](https://gitlab.com/ameliend/BlazeBot/commit/3d9566fbf2ffe741c5dfac14b344485a50c924a1))


### 🚀 Features

* add more custom reactions ([de9efd7](https://gitlab.com/ameliend/BlazeBot/commit/de9efd7e0e5c349e4a3c04424832b244a8e54f70))

## [2.1.1](https://gitlab.com/ameliend/BlazeBot/compare/v2.1.0...v2.1.1) (2023-03-30)


### 🦊 CI/CD

* renamed Dockerfile to dockerfile ([bfc8bfb](https://gitlab.com/ameliend/BlazeBot/commit/bfc8bfbaa44504569df3bb4003c6a9bdf0912907))
* update pipeline ([431eefb](https://gitlab.com/ameliend/BlazeBot/commit/431eefbb36a4708780f976ccd24a2f7916765d27))

## [2.1.0](https://gitlab.com/ameliend/BlazeBot/compare/v2.0.1...v2.1.0) (2023-03-30)


### 📔 Docs

* update README ([06a6ac7](https://gitlab.com/ameliend/BlazeBot/commit/06a6ac70c25ede7d54082b1234fe450993acdef1))


### 🦊 CI/CD

* fix pipeline ([12702fb](https://gitlab.com/ameliend/BlazeBot/commit/12702fbd518b7cbc037cd4e66e29cef8b187e048))
* update pipline with fly.io ([fe2f002](https://gitlab.com/ameliend/BlazeBot/commit/fe2f002ccdaf8f9af7cd10e11fa9cd6c2a964a08))


### 🚀 Features

* app deployed on https://fly.io ([7d680f4](https://gitlab.com/ameliend/BlazeBot/commit/7d680f4feb63ee42e02260112277e28598d8c96c))

## [2.0.1](https://gitlab.com/ameliend/BlazeBot/compare/v2.0.0...v2.0.1) (2023-03-30)


### 📔 Docs

* update docs ([278bdd3](https://gitlab.com/ameliend/BlazeBot/commit/278bdd301dc8d7057abdd11ce0c5d66ea2ea8f98))
* update README ([6dc5981](https://gitlab.com/ameliend/BlazeBot/commit/6dc5981abd65066821d0fda1a2c3cd9ba9250b49))


### 🦊 CI/CD

* udpate Procfile for Heroku ([57161a9](https://gitlab.com/ameliend/BlazeBot/commit/57161a93f16c6bd9d710ef829d0e1f2b2ae089a2))
* add heroku deploy ([e4ab213](https://gitlab.com/ameliend/BlazeBot/commit/e4ab21383ff74bbf5529141e1bc52ab65ed647ae))
* update pipeline ([61bc8bd](https://gitlab.com/ameliend/BlazeBot/commit/61bc8bd5dd993946f3b0e25050f4ce8712faa345))

## [2.0.0](https://gitlab.com/ameliend/BlazeBot/compare/v1.3.2...v2.0.0) (2023-03-30)


### 📔 Docs

* format using black ([b0c7cea](https://gitlab.com/ameliend/BlazeBot/commit/b0c7ceaeabd560589cb5ecc2b23b8acfc62c2284))
* remove useless help ([a34ff00](https://gitlab.com/ameliend/BlazeBot/commit/a34ff00b7c8255b5170296ae461404411a84c3d6))
* update readme ([b1917a1](https://gitlab.com/ameliend/BlazeBot/commit/b1917a13fa5dd67ac95ce6368bd20cf993503474))
* Update README.md ([9ea327d](https://gitlab.com/ameliend/BlazeBot/commit/9ea327dd3c54f75ced206a2562cf1a1901a176c3))


### 🦊 CI/CD

* add CHANGELOG and LICENSE ([26812ec](https://gitlab.com/ameliend/BlazeBot/commit/26812ec490d2fa2229644e8d430496bf190ee5bc))
* update semantic release ([3563d6e](https://gitlab.com/ameliend/BlazeBot/commit/3563d6e82266fe49f9e9d3d48392900e8600cfca))


### 🚀 Features

* add more CLEVERBOT_REPLACEMENTS ([69d3df4](https://gitlab.com/ameliend/BlazeBot/commit/69d3df407b984d9dd1535ebc4a04af9ae66bd2a0))
* using discord logger ([eb20472](https://gitlab.com/ameliend/BlazeBot/commit/eb204723b486606aa55a6a936cbe1d29c3c871a5))
* using NamedTuple ([57f6200](https://gitlab.com/ameliend/BlazeBot/commit/57f62004e9930dd2c4a3d62f37d297153bd31c72))
