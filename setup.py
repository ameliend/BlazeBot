# -*- coding: utf-8 -*-
"""Package setup file."""
from pathlib import Path

from setuptools import find_packages, setup

PROJECT_NAME = "Blazebot"
DESCRIPTION = "Hoppou Foundation Discord ChatBot."
AUTHOR = "Amelien Deshams"
AUTHOR_EMAIL = "gitlab.wgmhm@simplelogin.com"
SCRIPT_PATH = Path(__file__).resolve().parent
README_FILEPATH = SCRIPT_PATH / "README.md"
REQUIREMENTS_FILEPATH = SCRIPT_PATH / "requirements.txt"
if README_FILEPATH.is_file():
    LONG_DESCRIPTION = README_FILEPATH.read_text(encoding="utf8")
else:
    LONG_DESCRIPTION = "Unable to load README.md"
if REQUIREMENTS_FILEPATH.is_file():
    REQUIREMENTS = REQUIREMENTS_FILEPATH.read_text(encoding="utf8").splitlines()
else:
    REQUIREMENTS = []
VERSION = (SCRIPT_PATH / "blazebot" / "version.py").read_text().split('"')[1]

setup(
    name=PROJECT_NAME,
    version=VERSION,
    author=AUTHOR,
    author_email=AUTHOR_EMAIL,
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    project_urls={
        "Source": "https://gitlab.com/ameliend/kittysan-os",
    },
    classifiers=[  # https://pypi.org/classifiers/
        "Development Status :: 5 - Production/Stable",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3.9",
        "Topic :: Communications :: Chat",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
    ],
    packages=find_packages(exclude=["tests"]),
    license="MIT",
    python_requires=">=3.9",
    install_requires=REQUIREMENTS,
    include_package_data=True,
)
