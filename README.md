[![pipeline status](https://gitlab.com/ameliend/BlazeBot/badges/main/pipeline.svg)](https://gitlab.com/ameliend/BlazeBot/-/commits/main)
[![pylint](https://gitlab.com/ameliend/BlazeBot/-/jobs/artifacts/main/raw/public/badges/pylint.svg?job=pylint)](https://gitlab.com/ameliend/BlazeBot/-/commits/main)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![vscode-editor](https://badgen.net/badge/icon/visualstudio?icon=visualstudio&label)](https://code.visualstudio.com/)
[![fly](https://img.shields.io/badge/fly-io-blueviolet)](https://fly.io)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)


# BlazeBot

<p align="center">
  <img src="./blazebot/resources/logo.png">
</p>

A Chatbot designed to be used in **Hoppou Foundation discord server**, 
that reacts to messages in specific channels.

It is designed to look like Blazecreek.

## Chatbot channel

The main purpose of this bot is to be able to chat with it.
This Chatbot uses the unofficial and free API of **Cleverbot**.

<p align="left">
    <img src="./blazebot/resources/chatbot1.png">
</p>

It has custom reactions that can be set in the `settings.py` file.
A custom response dictionary is recorded and works by keyword. If these keywords are 
present in the player's sentence then the triggering of a custom answer is randomly drawn among 
the possible answers.

    CLEVERBOT_REPLACEMENTS: Will replace words in the Cleverbot answers.
    USER_REPLACEMENTS: Will replace words in the user answers.
    CUSTOM_CLEVERBOT_REACTIONS: Custom answers for the chatbot channel.
    CUSTOM_REACTIONS: Custom answers for every channels.
    GIF_REACTIONS: GIF tags for GIF reactions.
    MENTION_REACTIONS: Will pick randomly a reaction when @BlazeBot is mentionned.
    STATUS: Will pick randomly a status for the bot status.
    TIMEOUT_REACTIONS: Will pick randomly a reaction when there is a Timeout with the chatbot.

Example with "triggers": [["what", "what's"], ["hoppou"]]
With the input text: "What is a hoppou?" the function will return a custom answer because
**What**, and **hoppou** words are found in the given text. Same as ""**what's** a **hoppou**?",
"Hey, mister robot, **what** is a **hoppou**?", "BlazeBot, **what** the fuck is a **hoppou**?" ...
But will return None with the input text: "Where is the hoppou?" because the word **what** or 
**what's** are not
found. Same as "Who is that **hoppou**" ...

## Other channels

- BlazeBot reacts to messages in the **Poi** channel. *(One chance in 20)*

    <p align="left">
        <img src="./blazebot/resources/chatbot2.png">
    </p>

- BlazeBot reacts to messages in the **compliments** channel. *(One chance in 75)*
- BlazeBot reacts to messages in the **indev, art, finished** channels. *(One chance in 75)*
- BlazeBot reacts to custom keywords in **every** channels. *(One chance in 5)*
- BlazeBot reacts to his own mention in **every** channels. *(One chance in 2)*

    <p align="left">
        <img src="./blazebot/resources/chatbot3.png">
    </p>

## Fly.io Deploy

Using `flyctl.exe deploy` to deploy docker changes to fly.io.

## Evironment variables

It can be set with `flyctl.exe secrets set`, example `flyctl.exe secrets set CHATBOT_CHANNEL_ID=123456789`.

    CHATBOT_CHANNEL_ID
    POI_CHANNEL_ID
    COMPLIMENT_CHANNEL_ID
    INDEV_CHANNEL_ID
    FINISHED_CHANNEL_ID
    ART_CHANNEL_ID
    TENOR_TOKEN: The Tenor GIF API Token.
    BOT_TOKEN: The Discord bot Token.
    SERVER_ID: The Hoppou Foundation discord server ID.
