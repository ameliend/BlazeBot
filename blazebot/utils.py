import os
from asyncio import sleep

import discord


def find_emoji(client: discord.Client, emoji_name: str):
    """Find the emoji with the emoji_name in the discord server with the SERVER_ID environment variable.

    Parameters
    ----------
    client : discord.Client
        Represents a client connection that connects to Discord.
        This class is used to interact with the Discord WebSocket and API.
    emoji_name : str
        The name of the emoji you want to find.

    Returns
    -------
        The emoji object
    """
    guild = discord.utils.find(lambda g: g.id == int(os.environ["SERVER_ID"]), client.guilds)
    return discord.utils.get(guild.emojis, name=emoji_name)


async def send_message_with_typing(channel: discord.channel.TextChannel, text: str):
    """Send a message to a channel, and simulates typing for the duration of the message.

    Parameters
    ----------
    channel : discord.channel.TextChannel
        The channel to send the message to.
    text : str
        The text to send.
    """
    text_lengh = len(text.split())
    typing_delay = text_lengh * 0.8
    async with channel.typing():
        await sleep(typing_delay)
    await channel.send(text)
