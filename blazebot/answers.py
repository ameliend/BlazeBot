import logging
import os
import random
import re
import string
from typing import Optional

import settings
from cleverbotfreeapi import cleverbot
from TenGiphPy import Tenor

logger = logging.getLogger("discord")
tenor = Tenor(os.getenv("TENOR_TOKEN"))


def from_custom_reactions(text: str, reaction_dict: dict) -> Optional[str]:
    """Check if a message content can trigger a custom predefined response from a given reaction_dict.

    It searches for triggers in the given reaction_dict.
    Each trigger is a list of words that must be present in the message text for the corresponding response
    to be returned.

    If the message text matches all triggers for each custom reaction by counting the number of triggers
    using regular expressions, a random response is selected from the reaction_dict predefined
    responses for that trigger.

    Parameters
    ----------
    text : str
        The message content.
    reaction_dict : dict
        The custom reaction dict.

    Returns
    -------
    Optional[str]
        The custom reaction.
    """
    for custom_reaction in reaction_dict.values():
        matches = sum(
            any((re.search(rf"\b{word}\b", text, re.IGNORECASE) for word in words))
            for words in custom_reaction.triggers
        )
        if matches == len(custom_reaction.triggers):
            answer = random.choice(custom_reaction.answers)
            logger.info("Answer from custom reactions: %s", answer)
            return answer
    return None


def from_cleverbot(text: str) -> str:
    """Return an answer from Cleverbot, with session "hoppoufoundation".

    It also replace some words in the Cleverbot answer from the CLEVERBOT_REPLACEMENTS dict in settings.py.

    Parameters
    ----------
    text : str
        The message content.

    Returns
    -------
    Optional[str]
        The Cleverbot answer.
    """
    if (answer := cleverbot(text, session="hoppoufoundation")) in ["<html", "Hello from Cleverbot\\n"]:
        logger.warning("Too many requests. Timeout required.")
        return None
    text = alternate(answer)
    logger.info("Answer from Clerverbot: %s", text)
    return text


def alternate(text: str) -> str:
    """Alternate all instances of words in the text with their corresponding values in a dictionary.

    Parameters
    ----------
    text : str
        The message content.

    Returns
    -------
    str
        The text with the words replaced.
    """
    for word in text.translate(str.maketrans("", "", string.punctuation)).split(" "):
        if word.lower() in settings.CLEVERBOT_REPLACEMENTS:
            text = re.sub(rf"\b{word}\b", settings.CLEVERBOT_REPLACEMENTS[word.lower()], text)
            logger.info("Altered string: %s", f"{word} > {settings.CLEVERBOT_REPLACEMENTS[word.lower()]}")
    return text


def with_gif(text: str) -> Optional[str]:
    """Check if a message content can trigger a GIF reaction from the GIF_REACTIONS dict in settings.py.

    If any of the words in the given text are in a dictionary of words that should be responded to with a
    GIF, it will return a random GIF from the Tenor API.

    Parameters
    ----------
    text : str
        The message content.

    Returns
    -------
    Optional[str]
        A GIF link.
    """
    for word in text.translate(str.maketrans("", "", string.punctuation)).split(" "):
        if word.lower() in settings.GIF_REACTIONS:
            search_tags = settings.GIF_REACTIONS[word.lower()]
            logger.info("Answer with GIF: %s", search_tags)
            return tenor.random(search_tags)
    return None
