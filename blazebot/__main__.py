"""Blazebot is a chatbot that reacts to messages in specific channels in the Hoppou Foundation discord server.
It is designed to look like Blazecreek.
The bot changes its status randomly every 250 seconds and reacts to messages in the following channels:

CHATBOT_CHANNEL_ID: reacts to messages in the channel where the chatbot is active.
POI_CHANNEL_ID: reacts to messages in the Poi channel.
COMPLIMENT_CHANNEL_ID: reacts to messages in the channel where users can share compliments.
INDEV_CHANNEL_ID: reacts to messages in the channel where users can share their work that is still in
development.
FINISHED_CHANNEL_ID: reacts to messages in the channel where users can share their finished work.
ART_CHANNEL_ID: reacts to messages in the channel where users can share their art.

The reactions are handled by functions defined in the "reactions" module. 
The bot's token and channels IDs are obtained from environment variables.
"""
import os
import random

import discord
import reactions
import settings
from client import client
from discord.ext import tasks

CHANNEL_REACTIONS = {
    int(os.environ["CHATBOT_CHANNEL_ID"]): reactions.for_chatbot_channel,
    int(os.environ["POI_CHANNEL_ID"]): reactions.for_poi_channel,
    int(os.environ["COMPLIMENT_CHANNEL_ID"]): reactions.for_compliments_channel,
    int(os.environ["INDEV_CHANNEL_ID"]): reactions.for_creations_channels,
    int(os.environ["FINISHED_CHANNEL_ID"]): reactions.for_creations_channels,
    int(os.environ["ART_CHANNEL_ID"]): reactions.for_creations_channels,
}


@client.event
async def on_ready():
    """When the client is ready, it changes the status of the bot every 250 seconds."""
    change_status.start()


@tasks.loop(seconds=250)
async def change_status():
    """Set bot status randomly from STATUS list in settings.py."""
    await client.change_presence(
        status=discord.Status.do_not_disturb, activity=discord.Game(random.choice(settings.STATUS))
    )


@client.event
async def on_message(message: discord.message.Message):
    """Called when a Message is created and sent.

    Parameters
    ----------
    message : discord.message.Message
        The current message.
    """
    if message.author == client.user:
        return  # Avoid message reading event from the bot itself.
    if message.channel.id in CHANNEL_REACTIONS:
        reaction_func = CHANNEL_REACTIONS[message.channel.id]
        await reaction_func(message)
    elif client.user in message.mentions:
        await reactions.for_blazebot_mention(message)
    else:
        await reactions.for_every_channels(message)


client.run(os.environ["BOT_TOKEN"])
