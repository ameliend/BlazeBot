import random
from asyncio import sleep

import answers
import discord
import settings
import utils
from client import client


async def for_blazebot_mention(message: discord.message.Message):
    """React to a @BlazeBot mention.

    One chance in 2 to answer from the MENTION_REACTIONS list in settings.py.

    Parameters
    ----------
    message : discord.message.Message
        The current message.
    """
    if random.randint(0, 2) == 2:
        answer = random.choice(settings.MENTION_REACTIONS)
        await utils.send_message_with_typing(message.channel, answer)


async def for_chatbot_channel(message: discord.message.Message):
    """React to a message for the chatbot channel.

    The answer can be obtained from the "from_custom_reactions" or "from_cleverbot" function
    depending on the situation.

    If there is a problem with the answer, there is one chance in 5 to answer from the
    TIMEOUT_REACTIONS list in settings.py.

    The answer can include a GIF if the "with_gif" function returns a GIF link.

    Parameters
    ----------
    message : discord.message.Message
        The current message.
    """
    message_content = str(message.content)
    # Check if the answer is obtained from custom reactions.
    custom_reaction_dict = settings.CUSTOM_REACTIONS | settings.CUSTOM_CLEVERBOT_REACTIONS
    if not (answer := answers.from_custom_reactions(message_content, custom_reaction_dict)):
        # If not, get answer from Cleverbot.
        answer = answers.from_cleverbot(message_content)
    if not answer:  # If there is a problem with the answer.
        if random.randint(0, 5) == 5:
            answer = random.choice(settings.TIMEOUT_REACTIONS)
            await utils.send_message_with_typing(message.channel, answer)
        return
    await utils.send_message_with_typing(message.channel, answer)
    # Send a GIF if a GIF link is obtained from the answer.
    if gif := answers.with_gif(answer):
        await message.channel.send(gif)


async def for_poi_channel(message: discord.message.Message):
    """React to a message for the Poi channel.

    One chance in 20 to answer "Poi" in the Poi channel.

    Parameters
    ----------
    message : discord.message.Message
        The current message.
    """
    if random.randint(0, 20) == 20:
        await sleep(5)
        async with message.channel.typing():
            await sleep(3)
        await message.channel.send("Poi")


async def for_compliments_channel(message: discord.message.Message):
    """React to a message for the compliment channel.

    One chance in 75 to answer a PoiPat emoji in the compliment channel.

    Parameters
    ----------
    message : discord.message.Message
        The current message.
    """
    if random.randint(0, 75) == 75:
        if not (emoji := utils.find_emoji(client, "PoiPat")):
            return
        await sleep(5)
        await message.channel.send(emoji)


async def for_creations_channels(message: discord.message.Message):
    """React to a message for the creations channels.

    One chance in 75 to answer a HoppouWow or AmazePoi emoji in the creations channel.

    Parameters
    ----------
    message : discord.message.Message
        The current message.
    """
    if random.randint(0, 75) == 75:
        if not (emoji := utils.find_emoji(client, "HoppouWow")):
            return
        await sleep(5)
        await message.channel.send(emoji)
    elif random.randint(0, 75) == 75:
        if not (emoji := utils.find_emoji(client, "AmazePoi")):
            return
        await sleep(5)
        await message.channel.send(emoji)


async def for_every_channels(message: discord.message.Message):
    """React to a message for every channels, only if the message content trigger a custom reaction.

    One chance in 5 to answer from the CUSTOM_REACTIONS list in settings.py.

    Parameters
    ----------
    message : discord.message.Message
        The current message.
    """
    if random.randint(0, 5) == 5:
        if answer := answers.from_custom_reactions(str(message.content), settings.CUSTOM_REACTIONS):
            await utils.send_message_with_typing(message.channel, answer)
