from typing import NamedTuple


class CustomReactions(NamedTuple):
    """A named tuple that contains a list of triggers and a list of answers for setup custom reactions."""

    triggers: list
    answers: list


# Will replace words in the Cleverbot answers.
# The key is the word to be replaced with the value.
CLEVERBOT_REPLACEMENTS = {
    "human": "Hoppou",
    "humans": "Hoppous",
    "robot": "janky basterd",
    "bot": "janky basterd",
    "computer": "janky basterd",
    "cleverbot": "blazebot",
    "robots": "janky basterds",
    "bots": "janky basterds",
    "boy": "janky basterd",
    "male": "janky basterd",
    "man": "janky basterd",
    "girl": "Hoppou",
    "female": "Hoppou",
    "women": "Hoppou",
    "woman": "Hoppou",
    "married": "converted",
    "marry": "convert",
    "god": "Hoppou",
}

# Will replace words in the user answers.
# The key is the word to be replaced with the value.
USER_REPLACEMENTS = {
    "blazebot": "cleverbot",
}

# Custom answers for the chatbot channel.
# If these keywords are present in the users's sentence then the triggering of a custom answer is randomly
# drawn among the possible answers.

# Example with "triggers": [["what", "what's"], ["hoppou"]]
# With the input text: "What is a hoppou?" the function will return a custom answer because
# What, and hoppou words are found in the given text. Same as "what's a hoppou?",
# "Hey, mister robot, what is a hoppou?", "BlazeBot, what the fuck is a hoppou?" ...
# But will return None with the input text: "Where is the hoppou?" because the word what or what's are not
# found. Same as "Who is that hoppou" ...
CUSTOM_CLEVERBOT_REACTIONS = {
    "What is a hoppou": CustomReactions(
        triggers=[
            [
                "what",
                "what's",
            ],
            [
                "hoppou",
            ],
        ],
        answers=[
            "A Hoppou is a cute, small girl from the norther regions of the ocean",
            "The original Hoppou is based on the Northern Princess, from Kantai Collection",
            "The Hoppou base model we use, share one physical sign in particular, their eyes",
            "The original Hoppou is a short white character, usually three to four feet in height",
            "The Hoppous of VRChat are creatures as adorable as they are strange. They want your soul",
        ],
    ),
    "What is your name": CustomReactions(
        triggers=[
            [
                "your",
                "you're",
            ],
            [
                "name",
            ],
        ],
        answers=[
            "My name is BlazeBot",
            "I'm BlazeBot",
            "I am BlazeBot",
        ],
    ),
    "Who are you": CustomReactions(
        triggers=[
            [
                "who",
            ],
            [
                "are",
            ],
            [
                "you",
            ],
        ],
        answers=[
            "My name is BlazeBot",
            "I'm BlazeBot",
            "I am BlazeBot",
        ],
    ),
    "Are you a Hoppou": CustomReactions(
        triggers=[
            [
                "you",
            ],
            [
                "hoppou",
            ],
        ],
        answers=[
            "I am indeed a Hoppou",
            "Yes I am",
            "Yes, are you also a Hoppou?",
            "You're a Hoppou too?",
            "I am a Hoppou, so you are",
            "Poi",
        ],
    ),
    "I am a Hoppou": CustomReactions(
        triggers=[
            [
                "I",
                "I'm",
                "am",
            ],
            [
                "hoppou",
            ],
        ],
        answers=[
            "Awesome!",
            "I am a Hoppou too",
            "I am a Hoppou, so you are",
            "I really love hoppous",
            "Did you know that blazecreek have a hoppou tatoo?",
            "Poi",
        ],
    ),
    "Blazecreek": CustomReactions(
        triggers=[
            [
                "Blaze",
                "Blazecreek",
            ],
        ],
        answers=[
            "He is my clone",
            "I am him, but as a bot",
            "He's the founder and head of the Hoppou Foundation",
            "He's the founder and head of the Abyssal Poilice Department",
            "He is my daddy",
            "I captured Blazecreek once",
            "Yeah, I know who Blazecreek is",
            "Yeah Blazecreek",
            "I will now slap Blazecreek with a fish",
            "Blazecreek is just an imposter",
            "Blazecreek isnt real",
            "My name is Blazecreek",
            "Blaze Creek, Perche Township, Missouri 65256, USA",
            "I remember that he is a pretty good voice actor",
            "Blazecreek? he is a VTuber, right?",
            "I would like him to tell me stories",
            "When is the next poicast again?",
            "He's twitter is TheYoungBlazer",
            "He's TikTok is blazecreekfanboy",
            "Follow blazecreek on twitch https://www.twitch.tv/blazecreek",
            "He met Lolathon once",
        ],
    ),
    "Fae": CustomReactions(
        triggers=[
            [
                "Fae",
                "MischievousFae",
            ],
        ],
        answers=[
            "Fae is MY wife, not his",
            "I love you Fae",
            "She is my wife",
            "We are married",
            "No, I havn't forgot who Fae is",
            "My... his wife is Fae",
            "Listen, Blazecreek claims his wife is Fae, but he's wrong, she's mine, not his",
        ],
    ),
    "Your wife": CustomReactions(
        triggers=[
            [
                "you",
                "your",
            ],
            [
                "wife",
            ],
        ],
        answers=[
            "My wife is Fae",
            "MischievousFae is my wife",
            "I am married with Fae",
            "I haven't forgotten who my wife is, I'll have you know it's Fae",
            "My... his wife is Fae",
        ],
    ),
    "Drywall": CustomReactions(
        triggers=[
            [
                "drywall",
            ],
        ],
        answers=[
            "Drywall is food. With peanut butter.",
            "Drywall is good with peanut butter",
            "Drywall is a tasty snack",
            "Eat some drywall and you'll feel better",
            "My favorite food is drywall",
            "Yeah, have you eaten drywal before?",
            "Sprinkled drywall into Blazebot html soup",
            "Blazecreek be making drywall soup",
            "Would you rather souls or drywall",
            "How much drywall do you eat?",
        ],
    ),
    "Favorite food": CustomReactions(
        triggers=[
            [
                "favorite",
            ],
            [
                "food",
            ],
        ],
        answers=[
            "Drywall is my favorite food. With peanut butter.",
            "My favorite food is drywall",
            "Drywall, it's a tasty snack",
            "I like to eat drywall",
            "Drywall, have you eaten drywal before?",
            "Drywall soup, there are the best",
        ],
    ),
    "Favorite color": CustomReactions(
        triggers=[
            [
                "favorite",
            ],
            [
                "color",
            ],
        ],
        answers=[
            "My favorite color is purple",
            "Purple",
            "Purple, white, black",
        ],
    ),
    "Favorite character": CustomReactions(
        triggers=[
            [
                "favorite",
            ],
            [
                "character",
            ],
        ],
        answers=[
            "Rebecca from Cyberpunk Edgerunners",
            "My favorite character is Rebecca",
            "Rebecca is a very sharp-tongued young woman",
            "Rebecca has a tendency to be extreme and unpredictable",
            "I like to see Rebecca laughing",
        ],
    ),
    "Poi": CustomReactions(
        triggers=[
            [
                "poi",
            ],
        ],
        answers=[
            "Poi",
            "Poi?",
            "Poi poi",
            "Poi poi poi?",
            "Poi poi poi poi, poi poi poi poi",
        ],
    ),
}


# Custom answers for every channels.
# It works like CUSTOM_CLEVERBOT_REACTIONS.
CUSTOM_REACTIONS = {
    "Communism": CustomReactions(
        triggers=[
            [
                "communism",
                "communist",
            ],
        ],
        answers=[
            "I saw that",
            "Communism",
            "Did someone say communism?",
            "Yeet",
            "Fucking commie",
        ],
    ),
    "Nanachi": CustomReactions(
        triggers=[
            [
                "nanachi",
            ],
        ],
        answers=[
            "Eat it",
            "Eat the nanachi",
            "Listen, Nanachis don't have rights",
            "Yeet",
        ],
    ),
    "It really do be like that sometimes": CustomReactions(
        triggers=[
            [
                "it",
            ],
            [
                "really",
            ],
            [
                "do",
            ],
        ],
        answers=[
            "It really do",
            "It do be like that sometimes",
            "It really do be like that sometimes",
            "It doesn't seem like it be like that but it do",
        ],
    ),
}

# GIF tags for GIF reactions.
# The key is the word that trigger a GIF reaction, the value are the GIF tags to search with Tenor API.
GIF_REACTIONS = {
    "hug": "anime hug",
    "hugs": "anime hug",
    "headpat": "anime headpats",
    "headpats": "anime headpats",
    "slap": "fish slap",
    "slapped": "fish slap",
    "slaps": "fish slap",
    "punch": "anime punch",
    "punched": "anime punch",
    "kiss": "anime kiss",
    "kissed": "anime kiss",
    "kisses": "anime kiss",
    "yeet": "yeet",
    "eat": "anime eat",
    "eating": "anime eat",
}

# Will pick randomly a reaction when @BlazeBot is mentionned.
MENTION_REACTIONS = [
    "I agree",
    "I agree fully",
    "This is true",
    "This is fact",
    "It do be like that sometimes",
    "Hey it's me",
    "Yes, that's my name",
    "I am better than Blazecreek",
    "Blazecreek is just an imposter",
    "Yes, I am Blazebot",
    "Yo listen, don't ping me again",
    "Understandable",
    "Let's do drugs instead",
    "https://tenor.com/view/cat-scream-gif-24110282",
    "https://tenor.com/view/bang-funny-gun-handgun-anime-gif-18332168",
    "https://tenor.com/view/anime-pat-gif-22001971",
    "https://tenor.com/view/panical-weather-report-panic-yoshiisland-hunga-munga-gif-26281285",
    "https://tenor.com/view/anime-girl-tired-exhausted-gif-17772512",
    "https://tenor.com/view/karma-akabane-anime-assasination-classroom-gun-point-gun-gif-17236719",
    "https://tenor.com/view/anime-anna-gif-24010370",
    "https://tenor.com/view/chisato-nishikigi-lycoris-recoil-anime-sip-anime-drink-sipping-gif-26406910",
    "https://tenor.com/view/onii-chan-imouto-messing-hair-gif-11823783",
    "https://tenor.com/view/monogatari-koyomimonogatari-ononoki-monogatari-meme-ononoki-bruh-gif-18606469",
    "https://tenor.com/view/hug-gif-25588766",
    "https://tenor.com/view/yes-anime-yes-anime-si-nod-anime-gif-22893970",
    "https://tenor.com/view/anime-smug-gif-10195728",
    "https://tenor.com/view/scream-anime-shout-gif-15150171",
    "https://tenor.com/view/anime-waiting-for-text-waiting-for-your-reply-gif-14108959",
    "https://tenor.com/view/detective-conan-kogoro-mouri-richard-moore-think-thinking-gif-19055759",
    "https://tenor.com/view/you-wanna-get-high-towelie-stan-marsh-south-park-want-to-smoke-gif-22089216",
]

# Will pick randomly a status for the bot status.
STATUS = [
    "A POImmunist game",
    "VRChat",
    "SPOItify",
    "MC POIternal",
    "Poi",
    "Halo : The master POI edition",
    "OVR POIkit",
    "What is your favorite color",
]

# Will pick randomly a reaction when there is a Timeout with the chatbot.
TIMEOUT_REACTIONS = [
    "Sorry, I'm not here for the moment",
    "Sorry, I can't talk right now",
]
